SRC = Instruction.vala main.vala Package.vala Runner.vala utils.vala
FLAGS = --debug --vapidir=./vapi -X -O2
LIB = --pkg=posix --pkg=gio-2.0 
NAME = suprastore
PREFIX=~/.local/bin/

install:
	@valac $(FLAGS) $(LIB) $(SRC) -o $(NAME) 2>/dev/null 1>/dev/null 
	@echo "[SupraStore] Installation ..."
	@cp -f $(NAME) $(PREFIX) 

all:
	valac $(FLAGS) $(LIB) $(SRC) -o $(NAME) 

run: all
	./$(NAME)
