public class Instruction {
	public string []name_file;
	public string name_exec;
	public string []cmd;
	public string []dep;
	public double version;

	public Instruction(string name, string []cmd, string []files, string []dep) {
		this.dep = dep;
		this.name_file = files.copy();
		this.cmd = cmd;
		this.name_exec = name;
	}

	public bool make(){
		foreach(var url in name_file) {
			unowned string end_name = url.offset(url.last_index_of_char('/') + 1);
			download(url, end_name);
		}
		var pack = new Package (this);
		return (pack.install());
	}

	public static unowned Instruction? found(Instruction []list, ref string needle){
		foreach (var i in list) {
			if (i.name_exec == needle)
				return (!)i;
		}
		return null;
	}


	// get List to web
	public static Instruction []get_list(){
		download("https://gitlab.com/supraproject/suprastore/-/raw/master/list", "list");
		var stream = FileStream.open(SRC_DIR + "list", "r");
		if (stream == null)
			printerr("Impossible de lire %s", SRC_DIR + "list");
		Instruction []result = {};
		string []tab = {};
		string name = "";
		string []dep = {};
		string []cmd = {};
		double version = 0.0;

		while(!stream.eof()) {
			var line = read_line(ref stream);
			if (line == null)
				break ;
			if (line == "PROG") {
				name = read_line(ref stream);
				PKG_DIR = Environment.get_home_dir() + "/suprastore/" + name;
			}
			else if (line == "DEP") {
				while (!stream.eof()){
					line = read_line(ref stream);
					if (line == "ENDDEP")
						break;
					dep += line;
				}
			}
			else if (line == "CMD") {
				while (!stream.eof()){
					line = read_line(ref stream);
					if (line == "ENDCMD")
						break;
					cmd += line;
				}
			}
			else if (line == "LIST") {
				while (!stream.eof()){
					line = read_line(ref stream);
					if (line == "ENDLIST")
						break;
					tab += line;
				}
			}
			else if (line.has_prefix("VERSION")) {
				version = double.parse(line.offset(8));
			}
			else if (line == "ENDPROG") {
				var tmp = new Instruction(name, cmd, tab, dep);
				tmp.version = version;
				result += tmp;
				tab.resize(0);
				cmd.resize(0);
				dep.resize(0);
			}
		}
		return result;
	}
}
