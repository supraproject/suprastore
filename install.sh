#!/bin/bash
git clone https://gitlab.com/supraproject/suprastore tmp_git 2>/dev/null 1>/dev/null
make -C tmp_git install 2>/dev/null 1>/dev/null
rm -rf tmp_git
suprastore version 2>/dev/null
