class Package {
	public Package(Instruction app) {
		this.app = app;
		PKG_DIR = Environment.get_home_dir() + "/suprastore/" + app.name_exec;
	}

	// install subdirectory (PKG_DIR/app/info , PKG_DIR/app/bin)
	private void prepare() {
		Posix.mkdir(PKG_DIR, Posix.S_IRWXU | Posix.S_IRWXG | Posix.S_IRWXO);
		Posix.mkdir(PKG_DIR + "/bin", Posix.S_IRWXU | Posix.S_IRWXG | Posix.S_IRWXO);
		foreach(var i in app.cmd) {
			if (Posix.system(i) == -1)
				error("Fatal Error");
		}
		var fd = FileStream.open(PKG_DIR + @"/info", "w");
		if (fd.error () != 0){
			error("Can't create the folder suprastore\n");
		}
		fd.printf("VERSION %.2f\n", app.version);
		var list = generate_file_installed();
		foreach(var i in list) {
			fd.printf("%s\n", i);
		}
	}

	private void search(string path, ref List<string> result) {
		string name;

		try {
			var dir = Dir.open(path, 0);
			while ((name = dir.read_name ()) != null) {
				if (FileUtils.test (path + @"/$(name)", FileTest.IS_DIR)) {
					search(path + @"/$name", ref result);
				}
				else {
					result.append(path + @"/$name");
				}
			}
		}catch(Error e){
			printerr("\033[31m%s\033[0m", e.message);
		}

	}

	private string[] generate_file_installed(){
		list = new List<string>();
		search(PKG_DIR, ref list);
		string []result = {};
		foreach(var i in list) {
			result += i;
		}
		return result;
	}
	
	private static string[] get_file_installed(string app){
		string []result = {};
		try {
			var stream = FileStream.try_open(PKG_DIR + "/info", "r");
			var str = stream.read_line();

			while (true) {
				str = stream.read_line();
				if (str == null)
					break;
				result += str;
			}
		} catch(Error e){
			printerr(@"File error: $PKG_DIR/info");
			warning(e.message);
		}
		return result;
	}


	public void uninstall(){
		var list = get_file_installed(app.name_exec);
		foreach(var file in list) {
			var file_link = file.replace(PKG_DIR, PREFIX);
			if (Posix.system(@"rm -rf $file_link") == -1)
				error("Fatal Error");
			print("Remove: %s\n", file_link);
		}
		if (Posix.system(@"rm -rf $(PKG_DIR)") == -1)
			error("Fatal Error");
	}
	// install to Prefix
	public bool install(){
		prepare();
		foreach (var line in list){
			if (line != PKG_DIR + "/info"){
				Posix.unlink(line.replace(PKG_DIR, PREFIX));
				if (Posix.symlink(line, line.replace(PKG_DIR, PREFIX)) == -1)
					printerr("symlink Error");
			}
		}
		return true;
	}

	List<string> list;
	unowned Instruction app;
}
