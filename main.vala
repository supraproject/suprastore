using Posix;

public const double VERSION = 1.3;

public unowned string PREFIX;
public string SRC_DIR;
public string PKG_DIR;

void help(){
	print("--------------------\n");
	print("HELP SUPRASTORE\n");
	print("------------------\n");
	print("\033[2mlist\033[0m             Affiche la liste des programmes installable dans Suprastore.\n");
	print("\033[2mhelp\033[0m             Affiche l'aide pour les commandes de Suprastore.\n");
	print("\033[2mrun <nom>\033[0m        Execute le programme specifie par <nom>.\n");
	print("\033[2mforce <nom>\033[0m      reinstalle sa dernière version.\n");
	print("\033[2mupdate\033[0m           Met a jour tous les programmes installes dans Suprastore.\n");
	print("\033[2mupdate <nom>\033[0m     Met a jour uniquement le programme specifie par <nom>.\n");
	print("\033[2minstall <nom>\033[0m    Installe uniquement le programme specifie par <nom>.\n");
	print("\033[2muninstall <nom>\033[0m  Desinstalle  uniquement le programme specifie par <nom>.\n");
}

void main(string []args){
	prog(args);
}

public void prog(string []args)
{
	SRC_DIR = "/tmp/suprastore_" + Environment.get_user_name() + "/";

	string _prefix = Environment.get_home_dir() + "/.local";
	PKG_DIR = Environment.get_home_dir() + "/suprastore/";
	PREFIX = _prefix; 

	Posix.mkdir(SRC_DIR, S_IRWXU | S_IRWXG | S_IRWXO);
	Posix.mkdir(PKG_DIR, S_IRWXU | S_IRWXG | S_IRWXO);
	if (args.length == 1) {
		help();
		return ;
	}

	if (args.length == 2) {
		if (args[1] == "help") {
			help();
		}
		else if ("version" in args[1]) {
			print(suprastore + @"$(VERSION)\n");
		}
		else if (args[1] == "update") {
			var runner = new Runner(args[2:args.length]);
			runner.update_all();
		}
		else if (args[1] == "list") {
			var list = Instruction.get_list();
			print("voici la liste des programmes:\n");
			foreach (var i in list) {
				print("[%s] %.2f\n", i.name_exec, i.version.abs());
			}
		}
		else if (args[1] == "_list") {
			var list = Instruction.get_list();
			foreach (var i in list) {
				print("%s ", i.name_exec);
			}
		}
		else
			printerr("Command introuvable ou incomplete");
		return ;
	}
	var runner = new Runner(args[2:args.length]);
	switch (args[1]){
		case "_fake":
			runner.fake(args[2]);
			break;
		case "run":
			runner.run(args[2]);
			break;
		case "force":
			runner.force(args[2]);
			break;
		case "install":
			runner.install(args[2]);
			break;
		case "update":
			runner.update(args[2]);
			break;
		case "uninstall":
			runner.uninstall(args[2]);
			break;
	}
}
