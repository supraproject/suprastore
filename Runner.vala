public const string suprastore = "\033[31m[SupraStore] \033[0m: ";

public class Runner {
	public Runner(owned string []args) {
		this.args = (owned)args;
	}

	public void fake(string app) {
		string file = PREFIX + "/bin/" + app;
		if (FileUtils.test(file, GLib.FileTest.EXISTS))
			print(suprastore + "FakeCommand is already set\n");
		try {
			var stream = FileStream.try_open(file, "w");		
			FileUtils.chmod(file, 0755);
			stream.puts("#!/bin/bash\n");
			stream.printf(@"echo $(app) is not installed\n", app);
			stream.printf(@"echo run the command 'suprastore install $(app)'\n", app);
		}
		catch(Error e){
			warning(e.message);
		}
	}

	public void force(string app){
		var str = app;
		prog({"suprastore", "uninstall", str});
		prog({"suprastore", "install", str});
	}

	public void install(owned string app) {
		if (check(app) == true) {
			if (check_update(app))
				print(suprastore + @"you can update $(app) with `suprastore update $app`\n");
			return ;
		}
		print(suprastore + " installation de %s\n", app);
		var l = Instruction.found(list, ref app);
		if (l != null) {
			if (l.dep != null) {
				foreach (var i in l.dep) {
					if (check(i) == false){
						install(i);
					}
				}
			}
			l.make();
		}
		else {
			print(suprastore + " %s n'existe pas\n", app);
		}
	}

	private bool check(string app) {
		PKG_DIR = Environment.get_home_dir() + "/suprastore/";
		return (FileUtils.test(PKG_DIR + app, GLib.FileTest.EXISTS));
	}

	private bool check_update(owned string app) {
		var l = Instruction.found(list, ref app);
		if (l == null)
			return false;
		double version = 0.0;
		var stream = FileStream.open(GLib.Environment.get_home_dir() + "/suprastore/" + app + "/info", "r");
		var line = stream.read_line();
		unowned string str = line.offset(8);

		version = double.parse(str);
		if (version == l.version)
			return false;
		print(suprastore + @"%f --> %f\n\033[0m", version, l.version);
		return true;
	}

	private string []list_installed(){
		string []result = {};
		string name;
		try {
			var dir = GLib.Dir.open (Environment.get_home_dir() + "/suprastore", 0);
			while ((name = dir.read_name ()) != null) {
				result += name;
			}
		}catch(Error e){
			printerr(e.message);
		}
		return result;
	}

	public void update(owned string app) {
		if (app == "suprastore") {
			print(suprastore + "Updating suprastore...\n");
			Posix.system("curl https://gitlab.com/supraproject/suprastore/-/raw/master/install.sh 2>/dev/null | sh");
			return ;
		}
		if (check(app) == false) {
			print(suprastore + @"app n'existe pas\n\033[0m");
			return ;
		}
		if (check_update(app)) {
			force(app);
		}
		else 
			print(suprastore + @"Aucune Update pour $app\n\033[0m");
	}

	public void update_all() {
		var installed_list = list_installed();
		foreach(var i in installed_list) {
			update(i);
		}
	}

	public void run(string app){
		if (check(app) == false) {
			install(app);
		}
		if (check(app) == true)
			execute(app);
		else
			printerr("[run]: application inexistante\n");
	}

	private void execute(string app) {
		Posix.execvp(app, args);
		error("Execvp Binary not exist\n");
	}

	public void uninstall(owned string app){
		if (check(app) == false) {
			printerr(@"Can't uninstall app because $app don't exist\n");
			return ;
		}

		var l = Instruction.found(list, ref app);
		printerr("Remove %s\n", l.name_exec);
		if (l == null) {
			printerr("%s doesnt exist\n", app);
			return ;
		}
		new Package(l).uninstall();
	}

	private string []args;
	private Instruction[]? _list = null;
	private Instruction[]? list {
		get {
			if (_list == null)
				_list = Instruction.get_list();
			return _list;
		}
	}
}
