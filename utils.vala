public string? read_line(ref FileStream stream){
	string? result = stream.read_line();
	if (result == null)
		return null;
	result = result.replace("${SRC}", SRC_DIR);
	result = result.replace("${PKG}", PKG_DIR);
	return result;
}

public void download(string url_file, string to) {
	var dest = SRC_DIR + to;
	Posix.system(@"curl -s -o $(dest) $url_file");
}
